import jdk.jfr.Description;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

public class SuccessfulAuthorizationTest extends AbstractTest {

    @Test
    @DisplayName("Успешная авторизация на сайте")
    @Description("Успешная авторизация на сайте с использованием корректных исходных данных имени пользователя и пароля")
    @Tag("positiveTest")
    void test () throws InterruptedException {

        LoginPage loginPage = new LoginPage(getDriver());

        loginPage.loginIn("Trofimova", "b7faae95f5");

        Thread.sleep(1000);

        MainPage mainPage = new MainPage(getDriver());

        //ВЫБРАТЬ 2 ИЛИ 3 АССЕРТ
        Assertions.assertDoesNotThrow(() -> getDriver().manage().getCookieNamed("session_id").validate(),"Авторизация не была осуществлена успешно");
        Assertions.assertDoesNotThrow(() -> mainPage.getHome(),"Переход на страницу с постами не был осуществлен");
        Assertions.assertTrue(getDriver().getCurrentUrl().equals("https://test-stand.gb.ru/"), "Переход на страницу с постами не был осуществлен");

    }

}
