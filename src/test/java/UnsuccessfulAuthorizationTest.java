import jdk.jfr.Description;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

public class UnsuccessfulAuthorizationTest extends AbstractTest {

    @Test
    @DisplayName("Неуспешная авторизация на сайте")
    @Description("Неуспешная авторизация на сайте с использованием некорректных исходных данных имени пользователя и пароля")
    @Tag("negativeTest")
    void test () throws InterruptedException {

        LoginPage loginPage = new LoginPage(getDriver());

        loginPage.loginIn("Trofimova1", "b7faae95f5");

        Thread.sleep(1000);

        Assertions.assertTrue(getDriver().getCurrentUrl().contains("login"), "Осуществлен переход со страницы авторизации при использовании некорректных данных");
        Assertions.assertDoesNotThrow(() -> loginPage.getErrorCode401(),"Код ошибки отсутствует на странице");
    }

}
