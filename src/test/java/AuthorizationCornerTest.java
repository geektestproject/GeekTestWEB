import jdk.jfr.Description;
import org.junit.jupiter.api.*;

public class AuthorizationCornerTest extends AbstractTest {

    //Отсутствует ФЛК на странице авторизации в соответствии с требованиями.
    //Все не соответствующие ФЛК данные отправляются на бэк.
    //Заведены Баги 30-36.
    //На данный момент тесты помечены аннотацией Disabled. Аннотация будет снята после добавления функционала ФЛК в проект.
    //Также необходимо будет добавить ассерты на проверку ошибок при неуспешном прохождении ФЛК.

    @Test
    @Disabled("Тест временно помечен аннотацией Disabled, необходимо добавить тест в проект после добавления функции ФЛК полей на странице авторизации")
    @DisplayName("Успешное прохождение ФЛК на странице авторизации1")
    @Description("Успешное прохождение ФЛК на странице авторизации с использованием исходных данных имени пользователя длиной 20 латинских букв и пароля")
    @Tag("positiveTest")
    void test1 () throws InterruptedException {

        LoginPage loginPage = new LoginPage(getDriver());

        loginPage.loginIn("autotestautotestauto", "b7faae95f5");

        Thread.sleep(1000);

        Assertions.assertTrue(getDriver().getCurrentUrl().contains("login"), "Осуществлен переход со страницы авторизации при использовании некорректных данных");
        Assertions.assertDoesNotThrow(() -> loginPage.getErrorCode401(),"Код ошибки отсутствует на странице");

    }

    @Test
    @Disabled("Тест временно помечен аннотацией Disabled, необходимо добавить тест в проект после добавления функции ФЛК полей на странице авторизации")
    @DisplayName("Успешное прохождение ФЛК на странице авторизации2")
    @Description("Успешное прохождение ФЛК на странице авторизации с использованием исходных данных имени пользователя длиной 3 символа из цифр и пароля")
    @Tag("positiveTest")
    void test2 () throws InterruptedException {

        LoginPage loginPage = new LoginPage(getDriver());

        loginPage.loginIn("123", "b7faae95f5");

        Thread.sleep(1000);

        Assertions.assertTrue(getDriver().getCurrentUrl().contains("login"), "Осуществлен переход со страницы авторизации при использовании некорректных данных");
        Assertions.assertDoesNotThrow(() -> loginPage.getErrorCode401(),"Код ошибки отсутствует на странице");

    }

    @Test
    @Disabled("Тест временно помечен аннотацией Disabled, необходимо добавить тест в проект после добавления функции ФЛК полей на странице авторизации")
    @DisplayName("Успешное прохождение ФЛК на странице авторизации3")
    @Description("Успешное прохождение ФЛК на странице авторизации с использованием исходных данных имени пользователя и пароля из букв кириллицы, латиницы, спецсимволов и пробела")
    @Tag("positiveTest")
    void test3 () throws InterruptedException {

        LoginPage loginPage = new LoginPage(getDriver());

        loginPage.loginIn("Trofimova", "testтест $");

        Thread.sleep(1000);

        Assertions.assertTrue(getDriver().getCurrentUrl().contains("login"), "Осуществлен переход со страницы авторизации при использовании некорректных данных");
        Assertions.assertDoesNotThrow(() -> loginPage.getErrorCode401(),"Код ошибки отсутствует на странице");

    }

    @Test
    @Disabled("Тест временно помечен аннотацией Disabled, необходимо добавить тест в проект после добавления функции ФЛК полей на странице авторизации")
    @DisplayName("Неуспешное прохождение ФЛК на странице авторизации1")
    @Description("Неуспешное прохождение ФЛК на странице авторизации с использованием исходных данных имени пользователя длиной 2 символа и пароля")
    @Tag("negativeTest")
    void test4 () throws InterruptedException {

        LoginPage loginPage = new LoginPage(getDriver());

        loginPage.loginIn("au", "b7faae95f5");

        Thread.sleep(1000);

        Assertions.assertTrue(getDriver().getCurrentUrl().contains("login"), "Осуществлен переход со страницы авторизации при использовании некорректных данных");

    }

    @Test
    @Disabled("Тест временно помечен аннотацией Disabled, необходимо добавить тест в проект после добавления функции ФЛК полей на странице авторизации")
    @DisplayName("Неуспешное прохождение ФЛК на странице авторизации2")
    @Description("Неуспешное прохождение валидации на странице авторизации с использованием исходных данных имени пользователя длиной 21 символ и пароля")
    @Tag("negativeTest")
    void test5 () throws InterruptedException {

        LoginPage loginPage = new LoginPage(getDriver());

        loginPage.loginIn("123456789123456789123", "b7faae95f5");

        Thread.sleep(1000);

        Assertions.assertTrue(getDriver().getCurrentUrl().contains("login"), "Осуществлен переход со страницы авторизации при использовании некорректных данных");

    }

    @Test
    @Disabled("Тест временно помечен аннотацией Disabled, необходимо добавить тест в проект после добавления функции ФЛК полей на странице авторизации")
    @DisplayName("Неуспешное прохождение ФЛК на странице авторизации3")
    @Description("Неуспешное прохождение ФЛК на странице авторизации с использованием исходных данных имени пользователя из букв кирилицы и пароля")
    @Tag("negativeTest")
    void test6 () throws InterruptedException {

        LoginPage loginPage = new LoginPage(getDriver());

        loginPage.loginIn("Трофимова", "b7faae95f5");

        Thread.sleep(1000);

        Assertions.assertTrue(getDriver().getCurrentUrl().contains("login"), "Осуществлен переход со страницы авторизации при использовании некорректных данных");
    }

    @Test
    @Disabled("Тест временно помечен аннотацией Disabled, необходимо добавить тест в проект после добавления функции ФЛК полей на странице авторизации")
    @DisplayName("Неуспешное прохождение ФЛК на странице авторизации4")
    @Description("Неуспешное прохождение ФЛК на странице авторизации с использованием исходных данных имени пользователя с пробелом и пароля")
    @Tag("negativeTest")
    void test7 () throws InterruptedException {

        LoginPage loginPage = new LoginPage(getDriver());

        loginPage.loginIn("Trofimo va", "b7faae95f5");

        Thread.sleep(1000);

        Assertions.assertTrue(getDriver().getCurrentUrl().contains("login"), "Осуществлен переход со страницы авторизации при использовании некорректных данных");
    }

    @Test
    @Disabled("Тест временно помечен аннотацией Disabled, необходимо добавить тест в проект после добавления функции ФЛК полей на странице авторизации")
    @DisplayName("Неуспешное прохождение ФЛК на странице авторизации5")
    @Description("Неуспешное прохождение ФЛК на странице авторизации с использованием исходных данных имени пользователя с использованием спецсимволов и пароля")
    @Tag("negativeTest")
    void test8 () throws InterruptedException {

        LoginPage loginPage = new LoginPage(getDriver());

        loginPage.loginIn("Tro$imova", "b7faae95f5");

        Thread.sleep(1000);

        Assertions.assertTrue(getDriver().getCurrentUrl().contains("login"), "Осуществлен переход со страницы авторизации при использовании некорректных данных");
    }

    @Test
    @Disabled("Тест временно помечен аннотацией Disabled, необходимо добавить тест в проект после добавления функции ФЛК полей на странице авторизации")
    @DisplayName("Неуспешное прохождение ФЛК на странице авторизации6")
    @Description("Неуспешное прохождение ФЛК на странице авторизации с использованием исходных данных пустого имени пользователя и пароля")
    @Tag("negativeTest")
    void test9 () throws InterruptedException {

        LoginPage loginPage = new LoginPage(getDriver());

        loginPage.loginIn("", "b7faae95f5");

        Thread.sleep(1000);

        Assertions.assertTrue(getDriver().getCurrentUrl().contains("login"), "Осуществлен переход со страницы авторизации при использовании некорректных данных");

    }

    @Test
    @Disabled("Тест временно помечен аннотацией Disabled, необходимо добавить тест в проект после добавления функции ФЛК полей на странице авторизации")
    @DisplayName("Неуспешное прохождение ФЛК на странице авторизации7")
    @Description("Неуспешное прохождение ФЛК на странице авторизации с использованием исходных данных имени пользователя и пустого пароля")
    @Tag("negativeTest")
    void test10 () throws InterruptedException {

        LoginPage loginPage = new LoginPage(getDriver());

        loginPage.loginIn("Trofimova", "");

        Thread.sleep(1000);

        Assertions.assertTrue(getDriver().getCurrentUrl().contains("login"), "Осуществлен переход со страницы авторизации при использовании некорректных данных");

    }

}
