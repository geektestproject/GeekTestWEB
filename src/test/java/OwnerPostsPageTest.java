import jdk.jfr.Description;
import org.junit.jupiter.api.*;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;

public class OwnerPostsPageTest extends AbstractTest {

    @BeforeEach
    void authorizationAndGoToOwnerPostsPage() throws InterruptedException {

        LoginPage loginPage = new LoginPage(getDriver());

        loginPage.loginIn("Trofimova", "b7faae95f5");

        Thread.sleep(1000);

        Assertions.assertDoesNotThrow( ()-> driver.navigate().to("https://test-stand.gb.ru/?sort=createdAt&order=ASC"),"Страница недоступна");

        Thread.sleep(1000);

        MainPage mainPage = new MainPage(getDriver());

        if (!mainPage.getFirstPost().isDisplayed()){
            System.out.println("Посты отсутствуют на странице");
            driver.quit();
        }

    }

    @Test
    @DisplayName("Навигация на странице со своими постами")
    @Description("Навигация на странице со своими постами, переход между страницами постов, проверка сохранения настройки сортировки при переходе между страницами")
    @Tag("positiveTest")
    void test () throws InterruptedException {

        MainPage mainPage = new MainPage(getDriver());

        Assertions.assertDoesNotThrow(() -> mainPage.getBlogTitle(),"Заголовок 'Blog' отсутствует на странице");

        String post1 = mainPage.getFirstPost().getAttribute("href");

        getDriver().findElement(By.cssSelector("body")).sendKeys(Keys.CONTROL, Keys.END);

        Assertions.assertEquals("svelte-d01pfs disabled", mainPage.getPreviousPage().getAttribute("class"));

        mainPage.goToNextPostsPage();

        getDriver().findElement(By.cssSelector("body")).sendKeys(Keys.CONTROL, Keys.END);

        Thread.sleep(1500);

        Assertions.assertEquals("svelte-d01pfs", mainPage.getPreviousPage().getAttribute("class"));

        mainPage.goToPreviousPostsPage();

        getDriver().findElement(By.cssSelector("body")).sendKeys(Keys.CONTROL, Keys.END);

        Thread.sleep(1000);

        Assertions.assertEquals("svelte-d01pfs disabled", mainPage.getPreviousPage().getAttribute("class"));
        //Ассерт на данный момент не работает
        //Изначальная сортировка поста не сохраняется при переходе на следущую страницу и последущем переходе на первоначальную
        //Ассерт необходимо добавить в проект после исправления Бага сортировки
        //Assertions.assertEquals(mainPage.getFirstPost().getAttribute("href"),post1);

    }

    @Test
    @DisplayName("Проверка наличия картинки, заголовка и описания у постов")
    @Description("Проверка наличия картинки, заголовка и описания у первого поста на странице со своими постами")
    @Tag("positiveTest")
    void test2 () throws InterruptedException {

        MainPage mainPage = new MainPage(getDriver());

            Assertions.assertDoesNotThrow(() -> mainPage.getPictureFirstPost(), "У поста отсутствует картинка либо заглушка");
            Assertions.assertDoesNotThrow(() -> mainPage.getTitleFirstPost(), "У поста отсутствует заголовок");
            Assertions.assertDoesNotThrow(() -> mainPage.getDescriptionFirstPost(), "У поста отсутствует описание поста");

        getDriver().findElement(By.cssSelector("body")).sendKeys(Keys.CONTROL, Keys.END);

        Thread.sleep(1000);

        mainPage.goToNextPostsPage();

            Assertions.assertDoesNotThrow(() -> mainPage.getPictureFirstPost(), "У поста отсутствует картинка либо заглушка");
            Assertions.assertDoesNotThrow(() -> mainPage.getTitleFirstPost(), "У поста отсутствует заголовок");
            Assertions.assertDoesNotThrow(() -> mainPage.getDescriptionFirstPost(), "У поста отсутствует описание поста");

    }

}
