import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;

public class LoginPage extends AbstractPage {

    //@FindBy(css = ".mdc-text-field--label-floating > .mdc-text-field__input")
    @FindBy(xpath = "//input[@type='text']")
    private WebElement username;

    //@FindBy(css = ".mdc-text-field--focused > .mdc-text-field__input")
    @FindBy(xpath = "//input[@type='password']")
    private WebElement password;

    //FindBy(css = "css=.mdc-button__ripple")
    @FindBy(xpath = "//form[@id='login']/div[3]/button/div")
    private WebElement loginButton;

    @FindBy(xpath = "//h2[contains(.,'401')]")
    private WebElement errorCode401;

    public LoginPage(WebDriver driver){
        super(driver);
    }

    public void loginIn(String username, String password){

        Actions loginIn = new Actions(getDriver());
        loginIn.sendKeys(this.username,username)
                .sendKeys(this.password,password)
                .click(this.loginButton)
                .build()
                .perform();
    }

    public WebElement getErrorCode401() {
        return errorCode401;
    }

}
