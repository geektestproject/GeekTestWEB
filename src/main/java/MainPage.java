import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;

public class MainPage extends AbstractPage{

    @FindBy(xpath = "//span[contains(.,'Home')]")
    private WebElement Home;

    @FindBy(xpath = "//h1[contains(.,'Blog')]")
    private WebElement blogTitle;

    @FindBy(css = ".post:nth-child(1)")
    private WebElement FirstPost;

    @FindBy(css = ".post:nth-child(1) > .svelte-127jg4t:nth-child(1)")
    private WebElement pictureFirstPost;

    @FindBy(css = ".post:nth-child(1) > .svelte-127jg4t:nth-child(2)")
    private WebElement titleFirstPost;

    @FindBy(css = ".post:nth-child(1) > .description")
    private WebElement descriptionFirstPost;

    @FindBy(linkText = "Next Page")
    private WebElement NextPage;

    @FindBy(linkText = "Previous Page")
    private WebElement PreviousPage;

    public MainPage(WebDriver driver){
        super(driver);
    }

    public void goToNextPostsPage(){
        Actions goToNextPostsPage = new Actions(getDriver());
        goToNextPostsPage.click(this.NextPage)
                .pause(500l)
                .build()
                .perform();
    }
    public void goToPreviousPostsPage(){
        Actions goToPreviousPostsPage = new Actions(getDriver());
        goToPreviousPostsPage.click(this.PreviousPage)
                .pause(500l)
                .build()
                .perform();
    }

    public WebElement getHome() {
        return Home;
    }

    public WebElement getBlogTitle() {
        return blogTitle;
    }

    public WebElement getPreviousPage() {
        return PreviousPage;
    }

    public WebElement getFirstPost() {
        return FirstPost;
    }

    public WebElement getPictureFirstPost() {
        return pictureFirstPost;
    }

    public WebElement getTitleFirstPost() {
        return titleFirstPost;
    }

    public WebElement getDescriptionFirstPost() {
        return descriptionFirstPost;
    }

}
